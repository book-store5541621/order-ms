package com.example.orderms.service.impl;

import com.example.orderms.dto.request.OrderRequestDto;
import com.example.orderms.dto.response.OrderResponseDto;
import com.example.orderms.model.Orders;
import com.example.orderms.repository.OrderRepository;
import com.example.orderms.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;

    @Override
    public void create(OrderRequestDto orderRequestDto, UUID userId) {
        orderRepository.save(Orders.builder()
                .bookId(orderRequestDto.bookId())
                .userId(userId)
                .quantity(orderRequestDto.quantity())
                .build());
    }

    @Override
    public OrderResponseDto getById(UUID uuid) {
        return orderRepository.findById(uuid).map(o -> new OrderResponseDto(o.getBookId(), o.getQuantity(), o.getUserId())).orElseThrow();
    }


}
