package com.example.orderms.service;

import com.example.orderms.dto.request.OrderRequestDto;
import com.example.orderms.dto.response.OrderResponseDto;

import java.util.UUID;

public interface OrderService {
    void create(OrderRequestDto order, UUID userId);

    OrderResponseDto getById(UUID uuid);

}
