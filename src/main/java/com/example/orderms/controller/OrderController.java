package com.example.orderms.controller;

import com.example.orderms.dto.request.NotificationRequestDto;
import com.example.orderms.dto.request.OrderRequestDto;
import com.example.orderms.dto.response.BookResponseDto;
import com.example.orderms.dto.response.OrderResponseDto;
import com.example.orderms.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class OrderController {
    private final BookFeignClient bookFeignClient;
    private final OrderService orderService;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    @Value("${kafka.topic}")
    private String topic;

    @GetMapping
    public void confirmOrder(@RequestParam(value = "orderId") UUID orderId,
                             @RequestHeader("X-USER-ID") UUID userId,
                             @RequestHeader("X-USER-EMAIL") String userEmail) {
        kafkaTemplate.send(topic, new NotificationRequestDto("order confirmed", orderId, userId, userEmail));
    }

    @PostMapping
    public BookResponseDto create(@RequestBody OrderRequestDto orderRequestDto, @RequestHeader("X-USER-ID") UUID userId) {
        orderService.create(orderRequestDto, userId);
        BookResponseDto bookResponseDto = bookFeignClient.getById(orderRequestDto.bookId());
        return bookResponseDto;
    }

    @GetMapping("/{uuid}")
    public OrderResponseDto getById(@PathVariable UUID uuid) {
        return orderService.getById(uuid);
    }
}
