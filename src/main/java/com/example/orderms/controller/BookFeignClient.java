package com.example.orderms.controller;

import com.example.orderms.dto.response.BookResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

@FeignClient(name = "bookFeignClient", url = "http://localhost:8081/book")
public interface BookFeignClient {
    @GetMapping("/{uuid}")
    BookResponseDto getById(@PathVariable UUID uuid);
}
