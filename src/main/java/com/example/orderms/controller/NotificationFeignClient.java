package com.example.orderms.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "notificationFeignClient",url = "http://localhost:8082/notification")
public interface NotificationFeignClient {
    @GetMapping
    void sendNotification(@RequestParam(value = "orderId", required = false) String orderId);
}
