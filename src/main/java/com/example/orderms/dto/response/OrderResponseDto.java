package com.example.orderms.dto.response;

import java.util.UUID;

public record OrderResponseDto(UUID bookId, int quantity, UUID userId) {
}