package com.example.orderms.dto.response;

import java.math.BigDecimal;

public record BookResponseDto(String title, String author, BigDecimal price) {
}
