package com.example.orderms.dto.request;

import java.util.UUID;

public record OrderRequestDto(UUID bookId, int quantity) {
}
